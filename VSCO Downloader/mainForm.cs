﻿using System.Diagnostics;
using System.Windows.Forms;
using System.Net;
using System;

namespace VSCO_Downloader
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void downloadButton_Click(object sender, System.EventArgs e)
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    string html = client.DownloadString(pathBox.Text);

                    int from = html.IndexOf("responsiveUrl\":\"") + "responsiveUrl\":\"".Length;
                    int to = html.LastIndexOf("\",\"shareLink");
                    string path = html.Substring(from, to - from);

                    SaveFileDialog saveDialog = new SaveFileDialog();
                    saveDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    saveDialog.Filter = "Изображения (*.jpg)|*.jpg|Все (*.*)|*.*";
                    saveDialog.FilterIndex = 0;
                    saveDialog.FileName = path.Substring(path.Length - 10);
                    if (saveDialog.ShowDialog() == DialogResult.OK)
                    {
                        client.DownloadFile("http://" + path, @saveDialog.FileName.ToString());
                    }
                }
            }
            catch { }
        }
        #region CopyPasteAbout
        private void copyButton_Click(object sender, System.EventArgs e)
        {
            Clipboard.SetText(pathBox.Text);
        }
        private void pasteButton_Click(object sender, System.EventArgs e)
        {
            pathBox.Text = Clipboard.GetText();
        }
        private void aboutButton_Click(object sender, System.EventArgs e)
        {
            Process.Start("http://exdev.eu/vsco");
        }
        #endregion
    }
}
